import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FinanceComponent } from './finance/finance.component';
import { FinanceFormsComponent } from './finance-forms/finance-forms.component';
import {Routes, RouterModule} from '@angular/router';

const routes = [

{path: 'table', component: FinanceComponent},
{path: 'forms', component: FinanceFormsComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    FinanceComponent,
    FinanceFormsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule { }
