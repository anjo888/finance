export class Finance {
    constructor(
      public date: string,
      public name: string,
      public summ: number,
      public id?: number
    ) {}
  }

export interface Finances {
    finances: Finance[];
}
