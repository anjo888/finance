import { Component, Input } from '@angular/core';
import { Finance } from '../finance';


@Component({
  selector: 'app-finance',
  templateUrl: './finance.component.html',
  styleUrls: ['./finance.component.css']
})
export class FinanceComponent  {
  @Input() finance: Finance;
}
