import { Component } from '@angular/core';
import { Finance, Finances  } from './finance';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public finance: Finance[] = [
    new Finance('27.11.18, 14:47:00', 'Зарплата', 2000, 1)
  ];
  settingActive = 1;
  onAdd(finance: Finance) {
    this.finance.push(finance);
  }
}

