import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceFormsComponent } from './finance-forms.component';

describe('FinanceFormsComponent', () => {
  let component: FinanceFormsComponent;
  let fixture: ComponentFixture<FinanceFormsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceFormsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
