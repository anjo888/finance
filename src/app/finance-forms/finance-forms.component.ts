import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Finance } from '../finance';
import * as moment from 'moment';

@Component({
  selector: 'app-finance-forms',
  templateUrl: './finance-forms.component.html',
  styleUrls: ['./finance-forms.component.css']
})
export class FinanceFormsComponent implements OnInit {
  financeSumm: any;
  constructor() { }
  private id = 1;

  financeName = '';
  @Output() addFinance = new EventEmitter< Finance >();
  financeDate: any;

  ngOnInit() {
  }

  onAdd() {
    if (this.financeName === '') { return; }
    this.id = ++this.id;
    const finance = new Finance(
      moment().format('DD.MM.YY, HH:mm:ss'),
      this.financeName,
      this.financeSumm,
      this.id
    );
    this.addFinance.emit(finance);
    this.financeName = '';
    this.financeSumm = '';
  }

}
